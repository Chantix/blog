<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Ad
 *
 * @mixin \Eloquent
 */
class Ad extends Model
{
    protected $fillable = ['name', 'description', 'rate', 'position', 'user_id'];
}
