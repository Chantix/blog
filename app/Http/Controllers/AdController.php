<?php

namespace App\Http\Controllers;

use App\Ad;
use App\ProjectStaff;
use Auth;
use Illuminate\Http\Request;

class AdController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return array
     */
    public function list()
    {
        $ads = Ad::all();

        $formattedArray = ['ads' => []];
        foreach ($ads->toArray() as $index => $point) {
            $formattedArray['ads'][] = array($point)[0];
        }
        $formattedArray['labels'] = (new ProjectStaff())->getPositionLabels();

        return $formattedArray;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function create(Request $request)
    {
        $ad = new Ad();

        $request->validate([
            'name' => 'required',
            'rate' => 'numeric',
        ]);

        $ad->fill(request()->post());
        $ad->setAttribute('position', 1);
        $ad->setAttribute('user_id', Auth::user()->id);

        $createdAd = Ad::create($ad->attributesToArray());

        return ['id' => $createdAd->id];
    }

    /**
     * @return array
     */
    public function delete()
    {
        $ad = Ad::findOrFail(request()->post('id'));
        return ['success' => $ad->delete()];
    }
}
