<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ProjectStaff
 *
 * @mixin \Eloquent
 */
class ProjectStaff extends Model
{
    const POSITION_PROMOTER = 1;
    const POSITION_SUPERVISOR = 2;
    const POSITION_SHOP_ASSISTANT = 3;
    const POSITION_PROMOTIONAL_MODEL = 4;
    const POSITION_MODEL = 5;
    const POSITION_LIFE_SIZE_PUPPET = 6;
    const POSITION_ANIMATOR = 7;
    const POSITION_CHILDREN_ANIMATOR = 8;
    const POSITION_BARTENDER = 9;
    const POSITION_WAITER = 10;
    const POSITION_VISAGIST = 11;
    const POSITION_AQUAGRIMER = 12;
    const POSITION_ENGRAVER = 13;
    const POSITION_HOSTESS = 14;
    const POSITION_COURIER = 15;
    const POSITION_PHOTOGRAPHER = 16;
    const POSITION_COSMETOLOGIST = 17;
    const POSITION_AUDITOR = 18;
    const POSITION_HANDYMAN = 19;
    const POSITION_MYSTERY_SHOPPER = 20;
    const POSITION_MERCHANDISER = 21;
    const POSITION_SPRAYER = 22;

    public static function getPositionLabels(?int $position = null)
    {
        $positionList = [
            self::POSITION_PROMOTER => 'Промоутер',
            self::POSITION_SUPERVISOR => 'Супервайзер',
            self::POSITION_SHOP_ASSISTANT => 'Продавец-консультант',
            self::POSITION_PROMOTIONAL_MODEL => 'Промо-модель',
            self::POSITION_MODEL => 'Модель',
            self::POSITION_LIFE_SIZE_PUPPET => 'Ростовая кукла',
            self::POSITION_ANIMATOR => 'Аниматор',
            self::POSITION_CHILDREN_ANIMATOR => 'Детский аниматор',
            self::POSITION_BARTENDER => 'Бармен',
            self::POSITION_WAITER => 'Официант',
            self::POSITION_VISAGIST => 'Визажист',
            self::POSITION_AQUAGRIMER => 'Аквагример',
            self::POSITION_ENGRAVER => 'Гравировщик',
            self::POSITION_HOSTESS => 'Хостес',
            self::POSITION_COURIER => 'Курьер',
            self::POSITION_PHOTOGRAPHER => 'Фотограф',
            self::POSITION_COSMETOLOGIST => 'Косметолог',
            self::POSITION_AUDITOR => 'Аудитор/чекер',
            self::POSITION_HANDYMAN => 'Разнорабочий',
            self::POSITION_MYSTERY_SHOPPER => 'Тайный покупатель',
            self::POSITION_MERCHANDISER => 'Мерчендайзер',
            self::POSITION_SPRAYER => 'Спреер'
        ];

        return $position ? $positionList[$position] : $positionList;
    }
}
