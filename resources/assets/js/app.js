
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component('offers-component', require('./components/OffersComponent.vue'));
Vue.component('offer-form-component', require('./components/OfferFormComponent.vue'));

const app = new Vue({
    el: '#app',
    data: {
      offers: [],
    },

    created() {
      this.getModel();
    },

    methods: {
      getModel() {
        axios.get(
          '/ad/list'
        )
          .then((response) => {
            console.log(response.data.ads);
            this.offers = response.data.ads;
          })
          .catch((error) => {
            console.log(error);
          });
      },

      addNewOffer(newOfferList) {
        this.offers.push(newOfferList);
      },

      removeOffer(offerIndex) {
        console.log(offerIndex);
      }
    }
});
