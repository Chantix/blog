<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Points</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="./css/reset.css" rel="stylesheet" type="text/css">
        <link href="./css/app.css" rel="stylesheet" type="text/css">
        <link href="./css/points.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="app">
            <div class="points-wrap">
                <offers-component :offers="offers"></offers-component>
                <offer-form-component v-on:addoffer="addNewOffer"></offer-form-component>
            </div>
        </div>

    <script src="js/app.js" charset="utf-8"></script>
    </body>
</html>
