<?php

Route::get('/', function () {
    return view('welcome');
});


Route::get('/ad/list', 'AdController@list');
Route::get('/ad/create', 'AdController@create');
Route::post('/ad/create', 'AdController@create');
Route::post('/ad/delete', 'AdController@delete');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
